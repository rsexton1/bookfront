import { AppBar, Box, Button, Toolbar, Typography } from '@mui/material';
import React from 'react';
import { BrowserRouter, Link, Route, Switch } from 'react-router-dom'
import './App.css';
import Books from './components/book/Books';
import Authors from './components/author/Authors';

function App() {
  const useStyles = () => ({
    link: {
      marginLeft: 10,
      textDecoration: 'none',
    }
  })
  const classes = useStyles();
  
  return (
    <BrowserRouter>
      <Box sx={{ flexGrow: 1 }}>
        <AppBar position="sticky">
          <Toolbar>
            <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
              Home
            </Typography>
            <Link to="/books" style={classes.link}>
              <Button variant="contained">Books</Button>
            </Link>
            <Link to="/authors" style={classes.link}>
              <Button variant="contained">Authors</Button>
            </Link>
          </Toolbar>
        </AppBar>

        <Switch>
          <Route path="/books">
            <Books />
          </Route>
          <Route path="/authors">
            <Authors />
          </Route>
        </Switch>
      </Box>
    </BrowserRouter>
  );
}

export default App;
