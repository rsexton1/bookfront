import { AuthorDTO } from "./author.dto";

export interface UpdateBookDTO {
    title?: string;
    description?: string;
    author?: AuthorDTO;
    rating?: number;
    imageURL?: string;
}