export interface UpdateAuthorDTO {
    firstName?: string;
    lastName?: string;
}