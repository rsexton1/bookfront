export interface CreateAuthorDTO {
    firstName: string;
    lastName: string;
}