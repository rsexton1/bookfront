import { AuthorDTO } from "./author.dto";

export interface BookDTO {
    id: number;
    title: string;
    description: string;
    author: AuthorDTO;
    imageURL: string;
    rating: number;
}