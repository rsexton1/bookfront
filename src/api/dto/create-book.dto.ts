import { AuthorDTO } from "./author.dto";

export interface CreateBookDTO {
    title: string;
    description: string;
    author: AuthorDTO;
    imageURL: string;
}