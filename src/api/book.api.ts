import { CreateAuthorDTO } from "./dto/create-author.dto";
import { CreateBookDTO } from "./dto/create-book.dto";
import { UpdateAuthorDTO } from "./dto/update-author.dto";
import { UpdateBookDTO } from "./dto/update-book.dto";

export class BookAPI {
    public static async getAllBooks() {
        const resp = await fetch('http://localhost:3000/books', {
            method: "GET"
        })
        const data = await resp.json();
        return data;
    }

    public static async getOneBook(bookID: number) {
        const resp = await fetch(`http://localhost:3000/books/${bookID}`, {
            method: "GET"
        })
        const data = await resp.json();
        return data;
    }

    public static async createOneBook(createBookRequest: CreateBookDTO) {
        const resp = await fetch('http://localhost:3000/books', {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(createBookRequest),
        })
        const data = await resp.json();
        return data;
    }

    public static async updateOneBook(bookID: number, updateBookRequest: UpdateBookDTO) {
        const resp = await fetch(`http://localhost:3000/books/${bookID}`, {
            method: "PUT",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(updateBookRequest),
        })
        const data = await resp.json();
        return data;
    }

    public static async deleteOneBook(bookID: number) {
        await fetch(`http://localhost:3000/books/${bookID}`, {
            method: "DELETE",
        })
    }



    public static async getAllAuthors() {
        const resp = await fetch('http://localhost:3000/authors', {
            method: "GET"
        })
        const data = await resp.json();
        return data;
    }

    public static async getOneAuthor(authorID: number) {
        const resp = await fetch(`http://localhost:3000/authors/${authorID}`, {
            method: "GET"
        })
        const data = await resp.json();
        return data;
    }

    public static async createOneAuthor(createAuthorRequest: CreateAuthorDTO) {
        const resp = await fetch('http://localhost:3000/authors', {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(createAuthorRequest),
        })
        const data = await resp.json();
        return data;
    }

    public static async updateOneAuthor(authorID: number, updateAuthorRequest: UpdateAuthorDTO) {
        const resp = await fetch(`http://localhost:3000/authors/${authorID}`, {
            method: "PUT",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(updateAuthorRequest),
        })
        const data = await resp.json();
        return data;
    }

    public static async deleteOneAuthor(authorID: number) {
        await fetch(`http://localhost:3000/authors/${authorID}`, {
            method: "DELETE",
        })
    }
}