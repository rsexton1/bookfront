import { Grid, Typography } from "@mui/material";
import { useEffect, useState } from "react";
import { BookAPI } from "../../api/book.api";
import { BookDTO } from "../../api/dto/book.dto";
import Book from "./Book";

function AllBooks() {
    const [books, setBooks] = useState<BookDTO[]>([]);
    useEffect(() => {
        async function fetchAll() {
        const resp = await BookAPI.getAllBooks();
        setBooks(resp);
        }

        fetchAll();
    }, [])
      
    return (
        <Grid container direction="column" spacing={2} style={{ padding: 10 }}>
            <Grid item>
                <Typography variant="h3" style={{ marginLeft: 40, marginTop: 10 }}>
                    Books
                </Typography>
            </Grid>
            {books.map(book => {
                return (
                    <Grid item key={book.id}>
                        <Book data={book} />
                    </Grid>
                )
            })}
        </Grid>
    );
}

export default AllBooks;