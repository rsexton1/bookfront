import { Box, Button, Grid } from "@mui/material";
import { BrowserRouter, Link, Route, Switch, useRouteMatch } from "react-router-dom";
import AddBook from "./AddBook";
import AllBooks from "./AllBooks";
import EditBook from "./EditBook";



function Books() {
    const useStyles = () => ({
        link: {
            width: '100%',
            textDecoration: 'none',
            color: 'inherit'
        }
    });
    const classes = useStyles();
    
    let match = useRouteMatch();
    
    return (
        <BrowserRouter>
            <Grid container>
                <Grid item xs={9}>
                    <Switch>
                        <Route path={`${match.url}/add`}>
                            <AddBook />
                        </Route>
                        <Route path={`${match.url}/edit`}>
                            <EditBook />
                        </Route>
                        <Route path="/">
                            <AllBooks />
                        </Route>
                    </Switch>
                </Grid>
                <Grid item xs={3} style={{padding: 50}}>
                    <Box position="sticky">    
                        <Button variant="outlined" fullWidth={true} style={{marginBottom: 20}}>
                            <Link to="/books" style={classes.link}>View All Books</Link>
                        </Button>
                        <Button variant="outlined" fullWidth={true} style={{marginBottom: 20}}>
                            <Link to={`${match.path}/add`} style={classes.link}>Add Book</Link>
                        </Button>
                        <Button variant="outlined" fullWidth={true} style={{marginBottom: 20}}>
                            <Link to={`${match.path}/edit`} style={classes.link}>Edit Book</Link>
                        </Button>
                    </Box>
                </Grid>
            </Grid>
        </BrowserRouter>
    );
}

export default Books;