import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import { ButtonBase, Grid, IconButton, Paper, styled, Typography } from "@mui/material";
import { BookDTO } from "../../api/dto/book.dto";
import { BookAPI } from '../../api/book.api';

interface Props {
    data: BookDTO;
}

const Img = styled('img')({
    margin: '10%',
    display: 'block',
    maxWidth: '100%',
    maxHeight: '100%',
  });

function Book({data}: Props) {
    const deleteBook = async () => {
        await BookAPI.deleteOneBook(data.id);
        window.location.reload();
    }
    
    return (
        <Paper elevation={3} sx={{ p: 2, margin: 'auto', flexGrow: 1, marginLeft: 5, marginTop: 1.25 }}>
            <Grid container spacing={2}>
                <Grid item>
                    <ButtonBase sx={{ width: 198, height: 297 }} style={{ padding: '5%' }}>
                        <Img alt={`${data.title} Cover`} src={data.imageURL} />
                    </ButtonBase>
                </Grid>
                <Grid item xs={12} sm container>
                    <Grid item xs container direction="column" spacing={2}>
                        <Grid item xs>
                            <Typography gutterBottom variant="h6">
                                {data.title}
                            </Typography>
                            <Typography gutterBottom variant="body2">
                                by {data.author.firstName} {data.author.lastName}
                            </Typography>
                            <Typography variant="body2" color="text.secondary">
                                {data.description}
                            </Typography>
                        </Grid>
                        <Grid item>
                            <Typography sx={{ cursor: 'pointer' }} variant="body2">
                                Rating: {data.rating}/5
                            </Typography>
                        </Grid>
                    </Grid>
                    <Grid item xs={2} container direction="column" spacing={2} alignItems="flex-end">
                        <Grid item>
                            <IconButton
                                size="large"
                                edge="start"
                                color="inherit"
                                aria-label="menu"
                                sx={{ mr: 2 }}
                                onClick={deleteBook}
                            >
                                <DeleteForeverIcon />
                            </IconButton>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        </Paper>
      );
}

export default Book;