import { Grid, Typography, TextField, MenuItem } from "@mui/material";
import { useEffect, useState } from "react";
import { BrowserRouter, Link, Route, Switch, useHistory, useParams, useRouteMatch } from "react-router-dom";
import { BookAPI } from "../../api/book.api";
import { AuthorDTO } from "../../api/dto/author.dto";
import { BookDTO } from "../../api/dto/book.dto";
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import '../../styles.css';

interface FormValues {
    title: string;
    description: string;
    authorID: number;
    imageURL: string;
    rating: number;
}

function EditBook() {
    const [books, setBooks] = useState<BookDTO[]>([]);
    useEffect(() => {
        async function fetchAll() {
            const resp = await BookAPI.getAllBooks();
            setBooks(resp);
        }

        fetchAll();
    }, []);

    let match = useRouteMatch();
    
    return (
        <BrowserRouter>
            <Grid container direction="column" spacing={2} style={{ padding: 10 }}>
                <Grid item>
                    <Typography variant="h3" style={{ marginLeft: 40, marginTop: 10 }}>
                        Edit Book
                    </Typography>
                </Grid>
                <Grid item style={{ marginLeft: 30, marginTop: 10 }}>
                    <TextField
                        select
                        placeholder="Book"
                        label="Book"
                        style={{ width: '80%' }}
                        
                    >
                        {books.map((option) => (
                            <MenuItem key={option.id} value={option.id}>
                                <Link to={`${match.url}/${option.id}`} style={{ width: '100%', textDecoration: 'none', color: 'inherit' }}>
                                    {option.title}
                                </Link>
                            </MenuItem>
                        ))}
                    </TextField>
                </Grid>
                
                <Switch>
                    <Route path={`${match.path}/:bookID`}>
                        <EditBookFields />
                    </Route>
                </Switch>
            </Grid>
        </BrowserRouter>
    );
}

function EditBookFields() {
    let { bookID } = useParams<{bookID: string}>();
    const [book, setBook] = useState<BookDTO>(Object());
    useEffect(() => {
        async function fetchOne() {
            const resp = await BookAPI.getOneBook(Number(bookID));
            setBook(resp);
        }

        fetchOne();
    }, [bookID]);

    const [authors, setAuthors] = useState<AuthorDTO[]>([]);
    useEffect(() => {
        async function fetchAll() {
        const resp = await BookAPI.getAllAuthors();
        setAuthors(resp);
        }

        fetchAll();
    }, []);

    const history = useHistory();
    const updateBook = async (title: string, description: string, authorID: number, imageURL: string, rating: number) => {
        const resp = await BookAPI.updateOneBook(Number(bookID), { title, description, "author": {"id": authorID, "firstName": "no", "lastName": "no"}, imageURL, rating});
        console.log("Updated Author:", resp);
        history.push("../../books")
    }
    
    const initialValues: FormValues = { title: book.title, description: book.description, authorID: -1, imageURL: book.imageURL, rating: book.rating  }
    return (
        <Grid item style={{ marginLeft: 30, marginTop: 10 }}>
            <Formik
                initialValues={initialValues}
                validationSchema= { Yup.object({
                    title: Yup.string()
                        .max(64, 'Must be 64 characters or less'),
                    description: Yup.string()
                        .max(1024, 'Must be 1024 characters or less'),
                    authorID: Yup.number(),
                    imageURL: Yup.string()
                        .max(128, 'Must be 128 characters or less'),
                    rating: Yup.number()
                        .min(0, 'Must be between 0 and 5')
                        .max(5,'Must be between 0 and 5')
                }) }
                onSubmit={(values, { setSubmitting }) => {
                    updateBook(values.title, values.description, values.authorID, values.imageURL, values.rating);
                    setSubmitting(false);
                }}
            >
                <Form>
                    <label htmlFor="title">Title</label>
                    <Field name="title" type="text" />
                    <ErrorMessage name="title" />

                    <label htmlFor="description">Description</label>
                    <Field name="description" component="textarea" />
                    <ErrorMessage name="description" />

                    <label htmlFor="authorID">Author</label>
                    <Field name="authorID" component="select">
                        {authors.map((option) => (
                            <option key={option.id} value={option.id}>
                                {option.firstName} {option.lastName}
                            </option>
                        ))}
                    </Field>
                    <ErrorMessage name="authorID" />

                    <label htmlFor="imageURL">Image URL</label>
                    <Field name="imageURL" type="url" />
                    <ErrorMessage name="imageURL" />

                    <label htmlFor="rating">Rating</label>
                    <Field name="rating" type="number" />
                    <ErrorMessage name="rating" />

                    <button type="submit">Submit</button>
                </Form>
            </Formik>
        </Grid>
    );
}

export default EditBook;