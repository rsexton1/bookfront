import { Grid, Typography } from "@mui/material";
import { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import { BookAPI } from "../../api/book.api";
import { AuthorDTO } from "../../api/dto/author.dto";
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import '../../styles.css';

interface FormValues {
    title: string;
    description: string;
    authorID: number;
    imageURL: string;
}

function AddBook() {
    const history = useHistory();
    const createBook = async (title: string, description: string, authorID: number, imageURL: string) => {
        const resp = await BookAPI.createOneBook({ title, description, "author": {"id": authorID, "firstName": "no", "lastName": "no"}, imageURL });
        console.log("New Task:", resp);
        history.push("/books");
    }

    const [authors, setAuthors] = useState<AuthorDTO[]>([]);
    useEffect(() => {
        async function fetchAll() {
        const resp = await BookAPI.getAllAuthors();
        setAuthors(resp);
        }

        fetchAll();
    }, []);
    
    const initialValues: FormValues = { title: '', description: '', authorID: -1, imageURL: '' }
    return (
        <Grid container direction="column" spacing={2} style={{ padding: 10 }}>
            <Grid item>
                <Typography variant="h3" style={{ marginLeft: 40, marginTop: 10 }}>
                    Add Book
                </Typography>
            </Grid>
            <Grid item style={{ marginLeft: 30, marginTop: 10 }}>
                <Formik
                    initialValues={initialValues}
                    validationSchema= { Yup.object({
                        title: Yup.string()
                            .max(64, 'Must be 64 characters or less')
                            .required('Required'),
                        description: Yup.string()
                            .max(1024, 'Must be 1024 characters or less')
                            .required('Required'),
                        authorID: Yup.number()
                            .required('Required'),
                        imageURL: Yup.string()
                            .max(128, 'Must be 128 characters or less')
                            .required('Required')
                    }) }
                    onSubmit={(values, { setSubmitting }) => {
                        createBook(values.title, values.description, values.authorID, values.imageURL);
                        setSubmitting(false);
                    }}
                >
                    <Form>
                        <label htmlFor="title">Title</label>
                        <Field name="title" type="text" placeholder="Title" />
                        <ErrorMessage name="title" />

                        <label htmlFor="description">Description</label>
                        <Field name="description" component="textarea" placeholder="Description" />
                        <ErrorMessage name="description" />

                        <label htmlFor="authorID">Author</label>
                        <Field name="authorID" component="select">
                            <option disabled value={-1}>Author</option>
                            {authors.map((option) => (
                                <option key={option.id} value={option.id}>
                                    {option.firstName} {option.lastName}
                                </option>
                            ))}
                        </Field>
                        <ErrorMessage name="authorID" />

                        <label htmlFor="imageURL">Image URL</label>
                        <Field name="imageURL" type="url" placeholder="Image URL" />
                        <ErrorMessage name="imageURL" />

                        <button type="submit">Submit</button>
                    </Form>
                </Formik>
            </Grid>
        </Grid>
    );
}

export default AddBook;