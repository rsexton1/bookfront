import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import { Grid, IconButton, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Typography } from "@mui/material";
import { useEffect, useState } from "react";
import { BookAPI } from "../../api/book.api";
import { AuthorDTO } from "../../api/dto/author.dto";

function AllAuthors() {
    const [authors, setAuthors] = useState<AuthorDTO[]>([]);
    useEffect(() => {
        async function fetchAll() {
        const resp = await BookAPI.getAllAuthors();
        setAuthors(resp);
        }

        fetchAll();
    }, [])

    const deleteAuthor = async (id: number) => {
        await BookAPI.deleteOneAuthor(id);
        window.location.reload();
    }
      
    return (
        <Grid container direction="column" spacing={2} style={{ padding: 10 }}>
            <Grid item>
                <Typography variant="h3" style={{ marginLeft: 40, marginTop: 10 }}>
                    Authors
                </Typography>
            </Grid>
            <Grid item  style={{ marginLeft: 40, marginTop: 10 }}>
                <TableContainer component={Paper}>
                    <Table sx={{ minWidth: 650 }} aria-label="simple table">
                        <TableHead>
                            <TableRow>
                                <TableCell><b>Author</b></TableCell>
                                <TableCell align="right"><b>First Name</b></TableCell>
                                <TableCell align="right"><b>Last Name</b></TableCell>
                                <TableCell align="right"></TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {authors.map((author) => (
                                <TableRow
                                    key={author.id}
                                    sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                >
                                    <TableCell component="th" scope="row">
                                        {author.firstName} {author.lastName}
                                    </TableCell>
                                    <TableCell align="right">{author.firstName}</TableCell>
                                    <TableCell align="right">{author.lastName}</TableCell>
                                    <TableCell align="right">
                                        <IconButton
                                            size="large"
                                            edge="start"
                                            color="inherit"
                                            aria-label="menu"
                                            sx={{ mr: 2 }}
                                            onClick={() => deleteAuthor(author.id)}
                                        >
                                            <DeleteForeverIcon />
                                        </IconButton>
                                    </TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
            </Grid>
        </Grid>
    );
}

export default AllAuthors;