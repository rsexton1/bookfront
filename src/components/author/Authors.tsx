import { Box, Button, Grid } from "@mui/material";
import { BrowserRouter, Link, Route, Switch, useRouteMatch } from "react-router-dom";
import AddAuthor from "./AddAuthor";
import AllAuthors from "./AllAuthors";
import EditAuthor from "./EditAuthor";

function Authors() {
    const useStyles = () => ({
        link: {
            width: '100%',
            textDecoration: 'none',
            color: 'inherit'
        }
    });
    const classes = useStyles();
    
    let match = useRouteMatch();

    return (
        <BrowserRouter>
            <Grid container>
                <Grid item xs={9}>
                    <Switch>
                        <Route path={`${match.url}/add`}>
                            <AddAuthor />
                        </Route>
                        <Route path={`${match.url}/edit`}>
                            <EditAuthor />
                        </Route>
                        <Route path="/">
                            <AllAuthors />
                        </Route>
                    </Switch>
                </Grid>
                <Grid item xs={3} style={{padding: 50}}>
                    <Box position="sticky">    
                        <Button variant="outlined" fullWidth={true} style={{marginBottom: 20}}>
                            <Link to="/authors" style={classes.link}>View Authors Table</Link>
                        </Button>
                        <Button variant="outlined" fullWidth={true} style={{marginBottom: 20}}>
                            <Link to={`${match.path}/add`} style={classes.link}>Add Author</Link>
                        </Button>
                        <Button variant="outlined" fullWidth={true} style={{marginBottom: 20}}>
                            <Link to={`${match.path}/edit`} style={classes.link}>Edit Author</Link>
                        </Button>
                    </Box>
                </Grid>
            </Grid>
        </BrowserRouter>
    );
}

export default Authors;