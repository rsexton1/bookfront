import { Grid, Typography } from "@mui/material";
import { useHistory } from "react-router";
import { BookAPI } from "../../api/book.api";
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import '../../styles.css';

interface FormValues {
    firstName: string;
    lastName: string;
}

function AddAuthor() {
    const history = useHistory();
    const createAuthor = async (firstName: string, lastName: string) => {
        const resp = await BookAPI.createOneAuthor({ firstName, lastName });
        console.log("New Author:", resp);
        history.push("/authors");
    }
    
    const initialValues: FormValues = { firstName: '', lastName: '' }
    return (
        <Grid container direction="column" spacing={2} style={{ padding: 10 }}>
            <Grid item>
                <Typography variant="h3" style={{ marginLeft: 40, marginTop: 10 }}>
                    Add Author
                </Typography>
            </Grid>
            <Grid item style={{ marginLeft: 30, marginTop: 10 }}>
                <Formik
                    initialValues={initialValues}
                    validationSchema= { Yup.object({
                        firstName: Yup.string()
                            .max(15, 'Must be 15 characters or less')
                            .required('Required'),
                        lastName: Yup.string()
                            .max(20, 'Must be 20 characters or less')
                            .required('Required'),
                    }) }
                    onSubmit={(values, { setSubmitting }) => {
                        createAuthor(values.firstName, values.lastName);
                        setSubmitting(false);
                    }}
                >
                    <Form>
                        <label htmlFor="firstName">First Name</label>
                        <Field name="firstName" type="text" placeholder="First Name" />
                        <ErrorMessage name="firstName" />

                        <label htmlFor="lastName">Last Name</label>
                        <Field name="lastName" type="text" placeholder="Last Name" />
                        <ErrorMessage name="lastName" />

                        <button type="submit">Submit</button>
                    </Form>
                </Formik>
            </Grid>
        </Grid>
    );
}

export default AddAuthor;