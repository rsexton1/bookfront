import { Grid, Typography, TextField, MenuItem } from "@mui/material";
import { useEffect, useState } from "react";
import { BrowserRouter, Link, Route, Switch, useHistory, useParams, useRouteMatch } from "react-router-dom";
import { BookAPI } from "../../api/book.api";
import { AuthorDTO } from "../../api/dto/author.dto";
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import '../../styles.css';

interface FormValues {
    firstName: string;
    lastName: string;
}

function EditAuthor() {
    const [authors, setAuthors] = useState<AuthorDTO[]>([]);
    useEffect(() => {
        async function fetchAll() {
            const resp = await BookAPI.getAllAuthors();
            setAuthors(resp);
        }

        fetchAll();
    }, []);

    let match = useRouteMatch();

    return (
        <BrowserRouter>
            <Grid container direction="column" spacing={2} style={{ padding: 10 }}>
                <Grid item>
                    <Typography variant="h3" style={{ marginLeft: 40, marginTop: 10 }}>
                        Edit Author
                    </Typography>
                </Grid>
                <Grid item style={{ marginLeft: 30, marginTop: 10 }}>
                    <TextField
                        select
                        placeholder="Author"
                        label="Author"
                        style={{ width: '80%' }}
                        
                    >
                        {authors.map((option) => (
                            <MenuItem key={option.id} value={option.id}>
                                <Link to={`${match.url}/${option.id}`} style={{ width: '100%', textDecoration: 'none', color: 'inherit' }}>
                                    {option.firstName} {option.lastName}
                                </Link>
                            </MenuItem>
                        ))}
                    </TextField>
                </Grid>
                
                <Switch>
                    <Route path={`${match.path}/:authorID`}>
                        <EditAuthorFields />
                    </Route>
                </Switch>
            </Grid>
        </BrowserRouter>
    );
}

function EditAuthorFields() {
    let { authorID } = useParams<{authorID: string}>();
    const [author, setAuthor] = useState<AuthorDTO>(Object());
    useEffect(() => {
        async function fetchOne() {
            const resp = await BookAPI.getOneAuthor(Number(authorID));
            setAuthor(resp);
        }

        fetchOne();
    }, [authorID]);

    const history = useHistory();
    const updateAuthor = async (firstName: string, lastName: string) => {
        const resp = await BookAPI.updateOneAuthor(Number(authorID), { firstName, lastName });
        console.log("Updated Author:", resp);
        history.push('../../authors')
    }
    
    const initialValues: FormValues = { firstName: author.firstName, lastName: author.lastName }
    return (
        <Grid item style={{ marginLeft: 30, marginTop: 10 }}>
            <Formik
                initialValues={initialValues}
                validationSchema= { Yup.object({
                    firstName: Yup.string()
                        .max(15, 'Must be 15 characters or less'),
                    lastName: Yup.string()
                        .max(20, 'Must be 20 characters or less')
                }) }
                onSubmit={(values, { setSubmitting }) => {
                    updateAuthor(values.firstName, values.lastName);
                    setSubmitting(false);
                }}
            >
                <Form>
                    <label htmlFor="firstName">First Name</label>
                    <Field name="firstName" type="text" />
                    <ErrorMessage name="firstName" />

                    <label htmlFor="lastName">Last Name</label>
                    <Field name="lastName" type="text" />
                    <ErrorMessage name="lastName" />

                    <button type="submit">Submit</button>
                </Form>
            </Formik>
        </Grid>
    );
}

export default EditAuthor;